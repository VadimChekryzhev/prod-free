<?php

use yii\db\Migration;

/**
 * Handles the creation of table `balance`.
 */
class m171029_210404_create_balance_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('balance', [
      'id' => $this->primaryKey(),
      'id_user' => $this->integer()->notNull(),
      'balance' => $this->double(),
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('balance');
  }
}
