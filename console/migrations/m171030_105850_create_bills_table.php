<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bills`.
 */
class m171030_105850_create_bills_table extends Migration
{
  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('bills', [
      'id' => $this->primaryKey(),
      'user_from' => $this->integer()->notNull(),
      'balance_from' => $this->double()->defaultValue(0),
      'scope' => $this->double()->notNull(),
      'user_to' => $this->integer()->notNull(),
      'balance_to' => $this->double()->defaultValue(0),
      'translator' => $this->integer(),
      'date' => $this->dateTime()
    ]);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('bills');
  }
}
