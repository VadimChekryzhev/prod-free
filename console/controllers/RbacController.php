<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{

  public function actionInit()
  {
    $auth = Yii::$app->authManager;

//    удаляем старые данные
//    $auth->removeAll();

//    создаем новые роли
    $admin = $auth->createRole('admin');
    $user = $auth->createRole('user');

//    пишем в базу
    $auth->add($admin);
    $auth->add($user);

//    создаем разрешения
    $viewAdminPage = $auth->createPermission('viewAdminPage');
    $viewAdminPage->description = 'Просмотр админки';

//    пишем в базу
    $auth->add($viewAdminPage);

    $auth->addChild($admin, $viewAdminPage);

//    админ - пользователь с ID 1
    $auth->assign($admin, 1);
  }

}