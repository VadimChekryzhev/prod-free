<?php

use yii\widgets\LinkPager;

$this->title = 'Админка';
$this->params['breadcrumbs'][] = $this->title;
?>


<h2>Админка</h2>

<table class="history-bills">
  <tr>
    <th>ID пользователя</th>
    <th>Email</th>
    <th>Username</th>
    <th>Баланс, руб</th>
  </tr>
  <?= $active_str ?>
</table>


<div class="wrap-pagination">

  <?php
  echo LinkPager::widget([
    'pagination' => $pages,
  ]);
  ?>
</div>

