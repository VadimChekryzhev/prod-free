<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

  <h1><?= Html::encode($this->title) ?></h1>
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
  </p>

  <?= GridView::widget([
    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
//    'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",
    'tableOptions' => [
      'class' => 'table table-striped table-bordered'
    ],
    'columns' => [
//      ['class' => 'yii\grid\SerialColumn'],
      'id',
      'username',
      'email:email',
      'status_email:email',
//      'password',
      // 'activation',
      // 'status',
      // 'auth_key',
      // 'created_at',
      // 'updated_at',
      ['class' => 'yii\grid\ActionColumn',
        'header' => 'Зачислить',
        'headerOptions' => ['width' => '80'],
        'template' => '{pay}',
        'buttons' => [
          'pay' => function ($url, $model) {
            return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url);
          },
        ],
      ],
      ['class' => 'yii\grid\ActionColumn',
        'header' => 'Отправить',
        'headerOptions' => ['width' => '80'],
        'template' => '{send}',
        'buttons' => [
          'send' => function ($url, $model) {
            return Html::a('<span class="glyphicon glyphicon-minus"></span>', $url);
          },
        ],
      ],
      ['class' => 'yii\grid\ActionColumn',
        'header' => 'Действия',
        'headerOptions' => ['width' => '80'],
        'template' => '{view} {update} {delete}',
      ],
    ],
  ]); ?>
</div>
