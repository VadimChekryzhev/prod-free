<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Bills */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bills-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_from')->textInput() ?>

    <?= $form->field($model, 'balance_from')->textInput() ?>

    <?= $form->field($model, 'scope')->textInput() ?>

    <?= $form->field($model, 'user_to')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'balance_to')->textInput() ?>

    <?= $form->field($model, 'translator')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
