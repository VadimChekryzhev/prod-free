<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "bills".
 *
 * @property integer $id
 * @property integer $user_from
 * @property double $balance_from
 * @property double $scope
 * @property string $user_to
 * @property double $balance_to
 * @property string $translator
 */
class Bills extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'bills';
  }

  public function getUseremail()
  {
    return $this->hasOne(User::className(), ['id' => 'user_from']);
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['scope', 'user_to'], 'required'],
      [['user_from', 'user_to'], 'integer'],
      [['balance_from', 'scope', 'balance_to'], 'number'],
      [['translator'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'user_from' => 'User From',
      'balance_from' => 'Balance From',
      'scope' => 'Scope',
      'user_to' => 'User To',
      'balance_to' => 'Balance To',
      'translator' => 'Translator',
    ];
  }
}
