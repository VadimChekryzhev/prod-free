<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Bills;

/**
 * BillsSearch represents the model behind the search form about `backend\models\Bills`.
 */
class BillsSearch extends Bills
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_from'], 'integer'],
            [['balance_from', 'scope', 'balance_to'], 'number'],
            [['user_to', 'translator'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bills::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_from' => $this->user_from,
            'balance_from' => $this->balance_from,
            'scope' => $this->scope,
            'balance_to' => $this->balance_to,
        ]);

        $query->andFilterWhere(['like', 'user_to', $this->user_to])
            ->andFilterWhere(['like', 'translator', $this->translator]);

        return $dataProvider;
    }
}
