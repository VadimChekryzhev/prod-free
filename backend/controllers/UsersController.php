<?php

namespace backend\controllers;

use common\models\Bills;
use common\models\Helpfunc;
use frontend\models\Balance;
use frontend\models\SignupForm;
use Yii;
use common\models\User;
use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Transfer;
use yii\data\Pagination;


/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'actions' => [
              'logout',
              'index',
              'create',
              'update',
              'delete',
              'view',
              'pay',
              'send',
            ],
            'allow' => true,
            'roles' => ['admin'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
   * Lists all User models.
   * @return mixed
   */
  public function actionIndex()
  {

    $searchModel = new UsersSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single User model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $history = Bills::find()->where(['user_from' => $id])->orWhere(['user_to' => $id])->all();

    $history[0]->emailuserto;
    $history[0]->emailuserfrom;

    $active_str = '';

    foreach ($history as $row) {
      $active_str .= '<tr><td>' . $row['emailuserfrom'][0]['email'] . '</td><td>' . $row['emailuserto'][0]['email'] . '</td><td>' . $row['scope'] . '</td><td>' . $row['balance_from'] . '</td></tr>';
    }

    if ($active_str == '') {
      $active_str .= '<tr><td>n/a</td><td>n/a</td><td>n/a</td><td>n/a</td></tr>';
    }

    return $this->render('view', [
      'model' => $this->findModel($id),
      'active_str' => $active_str,
    ]);
  }

  /**
   * Creates a new User model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new SignupForm();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($user = $model->signup(1)) {

        $auth = Yii::$app->authManager;
        $userRole = $auth->getRole('user');
        $auth->assign($userRole, $user->getId());

        $score = new Balance();
        $score->id_user = $user->id;
        $score->balance = 0;
        $score->save();


        return $this->redirect(['/users']);
      } else {
        echo "невозможно добавить пользователя";
      }
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing User model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
   * Deletes an existing User model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }


  public function actionPay($id)
  {
    $user = $this->findModel($id);
    $username = $user->username;
    $email = $user->email;

    $model = new Transfer();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      User::transfer(
        $userFromId = Yii::$app->user->id,
        $userToEmail = $model->user_to,
        $scope = $model->scope,
        $model = $model
      );
    }

    return $this->render('translationToTheUser', compact('username', 'email', 'model'));
  }


  public function actionSend($id)
  {
    $user = $this->findModel($id);
    $username = $user->username;
    $email = $user->email;

    $model = new Transfer();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {

      User::transfer(
        $userFromId = $id,
        $userToEmail = $model->user_to,
        $scope = $model->scope,
        $model = $model
      );
    }

    return $this->render('send', compact('username', 'email', 'model'));
  }

  /**
   * Finds the User model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return User the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
