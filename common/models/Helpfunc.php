<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 0:51
 */

namespace common\models;

use Yii;
use yii\web\Controller;

class Helpfunc extends Controller
{

  public static function r2f($num)
  {
    return number_format((float)$num, 2, '.', '');
  }


  public static function debug($arr)
  {
    echo '<pre>' . print_r($arr, true) . '</pre>';
  }


  public static function transfer($userFromId, $userToEmail, $scope, $model)
  {
    $user_from = User::findOne($userFromId);
    $user_to = User::findOne(['email' => $userToEmail]);

    if ($scope > $user_from->getBalance()) {
      Yii::$app->session->setFlash('error_transfer', 'На балансе пользователя не хватает средств для перевода');
    } elseif ($scope <= 0) {
      Yii::$app->session->setFlash('error_transfer', 'Некорректное значение. Сумма перевода должна быть больше нуля.');
    } elseif ($user_to->email == $user_from->email) {
      Yii::$app->session->setFlash('error_transfer', 'Адрес отправления совпадает с адресом получения');
    } elseif ($user_to && $user_from) {
      if ($transfer = $model->transfer_user(Yii::$app->user->id, $user_to, $user_from)) {

        $transaction = Yii::$app->db->beginTransaction();
        try {
          if ($transfer->save() && $user_from->updateBalance(-$transfer->scope) && $user_to->updateBalance($transfer->scope)) {
            Yii::$app->session->setFlash('success_transfer', "Перевод стредств выполнен успешно. Сумма перевода {$transfer->scope} руб.");
            $transaction->commit();
          }
        } catch (\Throwable $e) {
          Yii::$app->session->setFlash('error_transfer', 'Ошибка при изменении балансов пользователей');
          $transaction->rollBack();
        }
      } else {
        Yii::$app->session->setFlash('error_transfer', 'Ошибка при переводе стредств');
      }
    } else {
      Yii::$app->session->setFlash('error_transfer', 'Невозможно найти отправителя/получателя');
    }

  }


  public static function sendMessage($email, $subject, $message)
  {
    $serverName = Yii::$app->request->serverName; //ваш сайт без http
    $headers = "Content-type: text/html; charset=utf-8 \r\n";
    $headers .= "From: support@" . $serverName . " <support@whoimi.ru>\r\n";
    $headers .= "Reply-To: support@" . $serverName . "\r\n";

    mail($email, $subject, $message, $headers);
  }


}