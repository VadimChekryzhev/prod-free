<?php

namespace common\models;

use frontend\models\Balance;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 */
class User extends ActiveRecord implements IdentityInterface
{
  const STATUS_DELETED = 0;
  const STATUS_ACTIVE = 10;


  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%user}}';
  }

  public function rules()
  {
    return [
      ['username', 'trim'],
      ['username', 'required'],
      ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
      ['username', 'string', 'min' => 2, 'max' => 255],

      ['email', 'trim'],
      ['email', 'required'],
      ['email', 'email'],
      ['email', 'string', 'max' => 255],
      ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

      ['password', 'required'],
      ['password', 'string', 'min' => 3],
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }

  //***********************************************************************************************
  public function getBillsfrom() {
    return $this->hasMany(Bills::className(), ['user_from' => 'id']);
  }
  //***********************************************************************************************
  public function getBillsto() {
    return $this->hasMany(Bills::className(), ['user_to' => 'id']);
  }
  //***********************************************************************************************

  /**
   * @inheritdoc
   */
//  public function rules()
//  {
//    return [
//      ['status', 'default', 'value' => self::STATUS_ACTIVE],
//      ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
//    ];
//  }

  /**
   * @inheritdoc
   */
  public static function findIdentity($id)
  {
    return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
  }

  /**
   * @inheritdoc
   */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

  /**
   * Finds user by username
   *
   * @param string $username
   * @return static|null
   */
  public static function findByUsername($username)
  {
    return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
  }


  /**
   * @inheritdoc
   */
  public function getId()
  {
    return $this->getPrimaryKey();
  }

  /**
   * @inheritdoc
   */
  public function getAuthKey()
  {
    return $this->auth_key;
  }

  /**
   * @inheritdoc
   */
  public function validateAuthKey($authKey)
  {
    return $this->getAuthKey() === $authKey;
  }

  /**
   * Validates password
   *
   * @param string $password password to validate
   * @return bool if password provided is valid for current user
   */
  public function validatePassword($password)
  {
    return Yii::$app->security->validatePassword($password, $this->password);
  }

  /**
   * Generates password hash from password and sets it to the model
   *
   * @param string $password
   */
  public function setPassword($password)
  {
    $this->password = Yii::$app->security->generatePasswordHash($password);
  }

  /**
   * Generates "remember me" authentication key
   */
  public function generateAuthKey()
  {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  /**
   * Generates new password reset token
   */
  public function generatePasswordResetToken()
  {
    $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
  }

  /**
   * Removes password reset token
   */
  public function removePasswordResetToken()
  {
    $this->password_reset_token = null;
  }


  //*********************************************************************************************

  public function getBalance()
  {
    $bills = Balance::find()->where(['id_user' => $this->id])->one();
    return $bills->balance;
  }

  public function updateBalance($scope)
  {
    $bills = Balance::find()->where(['id_user' => $this->id])->one();
    if ($bills) {
      $bills->balance = $bills->balance + round($scope, 2, PHP_ROUND_HALF_UP);
      return $bills->save() ? $bills : null;
    }
    return 0;




  }

  public static function transfer($userFromId, $userToEmail, $scope, $model)
  {
    $user_from = User::findOne($userFromId);
    $user_to = User::findOne(['email' => $userToEmail]);

    if ($scope > $user_from->getBalance()) {
      Yii::$app->session->setFlash('error_transfer', 'На балансе пользователя не хватает средств для перевода');
    } elseif ($scope <= 0) {
      Yii::$app->session->setFlash('error_transfer', 'Некорректное значение. Сумма перевода должна быть больше нуля.');
    } elseif ($user_to->email == $user_from->email) {
      Yii::$app->session->setFlash('error_transfer', 'Адрес отправления совпадает с адресом получения');
    } elseif ($user_to && $user_from) {
      if ($transfer = $model->transfer_user(Yii::$app->user->id, $user_to, $user_from)) {

        $transaction = Yii::$app->db->beginTransaction();
        try {
          if ($transfer->save() && $user_from->updateBalance(-$transfer->scope) && $user_to->updateBalance($transfer->scope)) {
            Yii::$app->session->setFlash('success_transfer', "Перевод стредств выполнен успешно. Сумма перевода {$transfer->scope} руб.");
            $transaction->commit();
          }
        } catch (\Throwable $e) {
          Yii::$app->session->setFlash('error_transfer', 'Ошибка при изменении балансов пользователей');
          $transaction->rollBack();
        }
      } else {
        Yii::$app->session->setFlash('error_transfer', 'Ошибка при переводе стредств');
      }
    } else {
      Yii::$app->session->setFlash('error_transfer', 'Невозможно найти отправителя/получателя');
    }

  }


}
