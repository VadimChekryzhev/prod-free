<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 0:30
 */

namespace common\models;

use frontend\models\Balance;
use yii\db\ActiveRecord;

/**
 * Bills model
 *
 * @property integer $id
 * @property integer $id_user
 * @property double $balance
 * @property double $scope
 * @property integer $user_to
 * @property integer $user_from
 */

class Bills extends ActiveRecord
{
  public static function tableName()
  {
    return '{{%bills}}';
  }

  public function getEmailuserfrom() {
    return $this->hasMany(User::className(), ['id' => 'user_from']);
  }

  public function getEmailuserto() {
    return $this->hasMany(User::className(), ['id' => 'user_to']);
  }





}