<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 1:20
 */

namespace frontend\controllers;

use frontend\models\Balance;
use frontend\models\Transfer;
use Yii;
use yii\web\Controller;
use common\models\Bills;
use common\models\Helpfunc;
use common\models\User;

class UserController extends Controller
{

  public function actionIndex()
  {
    return $this->render('index');
  }


  public function actionHistory()
  {

    $history = Bills::find()->where(['user_from' => Yii::$app->user->id])->orWhere(['user_to' => Yii::$app->user->id])->all();

//    Helpfunc::debug($history);
    $history[0]->emailuserto;
    $history[0]->emailuserfrom;

    $active_str = '';

    foreach ($history as $row) {
      if ($row['user_from'] == Yii::$app->user->id) $balance = $row['balance_from'];
      else $balance = $row['balance_to'];
      $active_str .= '<tr><td>' . $row['emailuserfrom'][0]['email'] . '</td><td>' . $row['emailuserto'][0]['email'] . '</td><td>' . $row['scope'] . '</td><td>' . $balance . '</td></tr>';
    }

    if ($active_str == '') {
      $active_str .= '<tr><td>n/a</td><td>n/a</td><td>n/a</td><td>n/a</td></tr>';
    }

    return $this->render('history', [
      'active_str' => $active_str,
    ]);

  }


  public function actionTransfer()
  {
    $model = new Transfer();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {

      User::transfer(
        $userFromId = Yii::$app->user->id,
        $userToEmail = $model->user_to,
        $scope = $model->scope,
        $model = $model
      );

    }

    return $this->render('transfer', compact("model"));
  }

}