<?php

namespace frontend\controllers;

use common\models\Bills;
use common\models\Helpfunc;
use frontend\models\Balance;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Site controller
 */
class SiteController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout', 'signup'],
        'rules' => [
          [
            'actions' => ['signup'],
            'allow' => true,
            'roles' => ['?'],
          ],
          [
            'actions' => ['logout'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return mixed
   */
  public function actionIndex()
  {
    return $this->render('index');
  }

  /**
   * Logs in a user.
   *
   * @return mixed
   */
  public function actionLogin()
  {
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    } else {
      return $this->render('login', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Logs out the current user.
   *
   * @return mixed
   */
  public function actionLogout()
  {
    Yii::$app->user->logout();
    return $this->goHome();
  }

  /**
   * Signs user up.
   *
   * @return mixed
   */
  public function actionSignup()
  {
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new SignupForm();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($user = $model->signup()) {

        $account = new Balance();
        $account->id_user = $user->id;
        if ($account->id_user == 1) {
          $account->balance = 1000;
        } else {
          $account->balance = 0;
        }
        $account->save();

        $auth = Yii::$app->authManager;
        $userRole = $auth->getRole('user');
        $auth->assign($userRole, $user->getId());

        $url = Url::home(true) . 'activation/' . $user->activation;

        Helpfunc::sendMessage(
          $email = $user->email,
          $subject = "Поздравляем с регистрацией",
          $message = "<p>Поздравялем с регистрацией на сайте</p>
                      <p>Для подтверждения регистрации пройдите по ссылке: <a href=$url>$url</a></p>"
        );

        Yii::$app->session->setFlash('email_confirm', 'Для дальнейшего входа в систему Вам необходимо подтвердить свой email');


        return $this->goHome();
      }
    }

    return $this->render('signup', compact('model'));
  }


  public function actionActivation()
  {
    $code = Yii::$app->request->get('code');
    $code = Html::encode($code);

    $find = User::find()->where(['activation' => $code])->one();
    $find->status_email = 1;

    if ($find) {
      $transaction = Yii::$app->db->beginTransaction();
      try {
        if ($find->save()) {
          $text = '<p>Поздравляю! Ваш e-mail успешно подтвержден!</p>';
          $transaction->commit();

          return $this->render('activation', [
            'text' => $text
          ]);
        }
      } catch (\Throwable $e) {
        $transaction->rollBack();
      }

    }

    $absoluteHomeUrl = Url::home(true);
    return $this->redirect($absoluteHomeUrl, 303); //на главную
  }

}
