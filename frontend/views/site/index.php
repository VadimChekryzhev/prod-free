<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

  <div class="body-content">

    <h2>Добро пожаловать!</h2>

    <?php if (Yii::$app->session->hasFlash('email_confirm')) : ?>
      <div class="alert alert-success alert-dismissable alert-registr">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?= Yii::$app->session->getFlash('email_confirm') ?>
      </div>
    <?php endif; ?>

    <div class="row">
      <div class="col-lg-12">
        <p>На этом сайте вы можете осуществлять денежные переводы внутри системы другим пользователям, зная только их <b>email адрес</b>. </p>
        <p>Основные правила:</p>
        <ol>
          <li>Перевод возможен если получатель зарегестрирован в системе</li>
          <li>Сумма перевода всегда положительное число больше нуля</li>
          <li>Сумма перевода может быть представлена числом формата XX.XX. Если дробная часть (число копеек) содержит более двух знаков,
            то оно будет <b>автоматически округлено до двух знаков после запятой</b>.</li>
        </ol>
      </div>
    </div>

  </div>
</div>
