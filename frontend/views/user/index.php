<?php

use yii\helpers\Html;
use common\models\Helpfunc;

?>

<div class="wraper">
  <h2>Личный кабинет</h2>

  <?= Html::a('Просмотреть историю счета', ['/user/history'], ['class' => 'custom-button long-button'])?>
  <?= Html::a('Перевести средства другому пользователю', ['/user/transfer'], ['class' => 'custom-button long-button'])?>

</div>