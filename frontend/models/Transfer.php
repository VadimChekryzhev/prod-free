<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 3:06
 */

namespace frontend\models;

use common\models\Bills;
use common\models\Helpfunc;
use Yii;
use yii\db\ActiveRecord;
use common\models\User;

class Transfer extends ActiveRecord
{

  public static function tableName()
  {
    return 'bills';
  }


  public function attributeLabels()
  {
    return [
      'user_to' => 'Email',
      'scope' => 'Сумма перевода',
    ];
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_to', 'scope'], 'trim'],
      [['user_to', 'scope'], 'required'],

      ['user_to', 'email'],
      ['user_to', 'validateUserto'],
      ['user_to', 'string', 'max' => 255],


      ['scope', 'double', 'min' => 0.01],
    ];
  }

  public function transfer_user($autor_id = null, $user_to, $user_from)
  {
    $new_transfer = new Bills();

    $new_transfer->user_from = $user_from->id;
    $new_transfer->user_to = $user_to->id;

    $new_transfer->scope = round($this->scope, 2, PHP_ROUND_HALF_UP);

    $new_transfer->balance_from = $user_from->getBalance() - $new_transfer->scope;
    $new_transfer->balance_to = $user_to->getBalance() + $new_transfer->scope;

    $new_transfer->date = date("Y-m-d H:i:s");
    $new_transfer->translator = $autor_id ? $autor_id : $new_transfer->user_from;

    return $new_transfer;
  }

  public function validateUserto($attribute, $params)
  {
    if ($this->user_to == Yii::$app->user->identity->email) {
      $this->addError($attribute, 'Вы не можете использовать себя в качестве получателя');
    }
  }




}