<?php
/**
 * Created by PhpStorm.
 * User: chvs
 * Date: 30.10.2017
 * Time: 4:09
 */

namespace frontend\models;


use yii\db\ActiveRecord;

class Balance extends ActiveRecord
{

  public static function tableName()
  {
    return '{{%balance}}';
  }

  public function updateBalance($balance)
  {

    $this->balance = $this->balance + $balance;

  }


}