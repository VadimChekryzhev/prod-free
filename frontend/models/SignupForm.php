<?php

namespace frontend\models;

use common\models\User;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use Yii;

/**
 * Signup form
 */
class SignupForm extends ActiveRecord
{

  public static function tableName()
  {
    return 'user';
  }

  public function attributeLabels() {
    return [
      'email' => 'Email',
      'username' => 'Логин',
      'password' => 'Пароль',
    ];
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      ['username', 'trim'],
      ['username', 'required'],
      ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
      ['username', 'string', 'min' => 2, 'max' => 255],

      ['email', 'trim'],
      ['email', 'required'],
      ['email', 'email'],
      ['email', 'string', 'max' => 255],
      ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

      ['password', 'required'],
      ['password', 'string', 'min' => 3],
    ];
  }

  /**
   * Signs user up.
   *
   * @return User|null the saved model or null if saving fails
   */
  public function signup($email_status = 0)
  {
    $user = new User();
    $user->username = $this->username;
    $user->email = $this->email;
    $user->status_email = $email_status;
    $user->setPassword($this->password);
    $user->generateAuthKey();
    $user->status = 10;
    $user->activation = md5($user->email.time());

    return $user->save() ? $user : null;
//    return $user;
  }



}
